                <div class="clearfix col-xs-12">
                    <p class="signature text-right small" style="margin-right:20px"><?php echo $t['meta']['start'].'<br/>'.$t['meta']['edit'] ?></p>
                </div>
            </div>
        </div>
        <a href="#" id="back-to-top" title="<?php echo $t['_Back to top'] ?>" ><span class="fa fa-fw fa-arrow-up"></span><span class="sr-only"><?php echo $t['_Back to top'] ?></span></a>
        </main>
    </body>
</html>
